package com.telequto.mutai.veribiz;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.text.format.Formatter;
import android.util.Log;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.lang.reflect.Method;

import static android.content.Context.WIFI_SERVICE;

/**
 * Created by mutai on 5/9/17.
 */

public class GHttpServer {
    ServletContextHandler handler;
    Server server;
    Context context;

    /**
     * Due to a bug in Android 2.x, it is recommended that you
     * do this. I have seen the bug only on the emulators.
     */
    static {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            System.setProperty("java.net.preferIPv6Addresses", "false");
        }
    }

    /**
     * Create the jetty server and let it listen at port 8080
     */
    GHttpServer(Context context) {

        this.context = context;

        server = new Server(8080); // The port is being set here
        handler = new ServletContextHandler();
        handler.setContextPath("/");
        handler.addServlet(new ServletHolder(new Servlet(context)), "/");
        server.setHandler(handler);
    }

    /*
     * We better run the server in a separate thread.
     */
    public void start() {
        new Thread() {
            public void run() {
                try {
                    server.start();
                } catch (Exception e) {
                    Log.e("SERVER_START", e.toString());
                }
            }
        }.start();
    }

    public String getIPAddress() {
        ConnectivityManager con = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        WifiManager wifi = (WifiManager) context.getSystemService(WIFI_SERVICE);
        if (con.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()) {
            return Formatter.formatIpAddress(wifi.getConnectionInfo().getIpAddress());
        } else {
            if (isSharingWiFi(wifi)) {
                //hotspot active. Return default hotspot IP which is 192.168.43.1
                return "192.168.43.1";
            } else{
                return "You are not connected to WIFI or hosting a WIFI hotspot";
            }
        }
    }

    private static boolean isSharingWiFi(final WifiManager manager)
    {
        try
        {
            final Method method = manager.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true); //in the case of visibility change in future APIs
            return (Boolean) method.invoke(manager);
        }
        catch (final Throwable ignored) {}

        return false;
    }

    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            Log.e("SERVER_STOP", e.toString());
        }
    }
}
