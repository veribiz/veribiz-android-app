package com.telequto.mutai.veribiz;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private String code;
    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        gps = new GPSTracker(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        code = getIntent().getStringExtra("code");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadDetails(code);
    }

    private void loadDetails(String code) {
        String ip = "http://192.168.43.85/veribiz/public/api/";
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("biz", code);
        final boolean canGetLocation = gps.canGetLocation();
        if(canGetLocation)
        {
            gps.getLocation();
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            params.put("gps", "on");
            params.put("lat", latitude);
            params.put("lng", longitude);
        }
        client.get(ip + "all", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                TextView text = (TextView) findViewById(R.id.info);
                text.setVisibility(View.VISIBLE);
                try {
                    setTitle("Business Details");
                    String data;
                    if(response.getBoolean("registered")){
                        data = "BUSINESS\n" + response.getString("name") + "\n\n";
                        data += "DESCRIPTION\n" + response.getString("description") + "\n\n";
                        data += "LOCATION\n" + response.getString("location") + "\n\n";
                        //String photo = response.getString("photo");
                        //loadPhoto(photo);
                        JSONArray licenseSubs = response.getJSONArray("licenseSubs");
                        data += "LICENSE SUBSCRIPTIONS\n\n";
                        if(licenseSubs.length() > 0){
                            for (int x = 0; x < licenseSubs.length(); x++) {
                                JSONObject licenseSub = licenseSubs.getJSONObject(x);
                                data += licenseSub.getString("name");
                                data += "\n";
                                data += "Fee: " + licenseSub.getString("fee");
                                data += "\n";
                                data += "From: " + licenseSub.getString("commencement");
                                data += "\n";
                                data += "To: " + licenseSub.getString("expiry");
                                data += "\n\n";
                            }
                        }
                        else
                        {
                            data += "No license subscriptions were found!";
                        }
                        data += "\n\n\n";
                        JSONArray permitSubs = response.getJSONArray("permitSubs");
                        data += "PERMIT SUBSCRIPTIONS\n\n";
                        if(permitSubs.length() > 0){
                            for (int x = 0; x < permitSubs.length(); x++) {
                                JSONObject permitSub = permitSubs.getJSONObject(x);
                                data += permitSub.getString("name");
                                data += "\n";
                                data += "Fee: " + permitSub.getString("fee");
                                data += "\n";
                                data += "From: " + permitSub.getString("commencement");
                                data += "\n";
                                data += "To: " + permitSub.getString("expiry");
                                data += "\n\n";
                            }
                        }
                        else
                        {
                            data += "No permit subscriptions were found!\n";
                        }
                        if (canGetLocation){
                            data += "\nDisparity in distance: " + response.getString("distance") +
                                    " metres";
                            if (response.getBoolean("distanceThresholdExceeded")) {
                                data = "!!!LOCATION THRESHOLD EXCEEDED!!!\n\n" + data;
                                text.setTextColor(Color.parseColor("#954120"));
                            }
                        }
                    }else{
                        data = "WARNING\n\n!!!BUSINESS IS NOT REGISTERED!!!";
                        text.setTextColor(Color.parseColor("#954120"));
                    }
                    text.setText(data);
                } catch (JSONException e) {
                    Snackbar.make(findViewById(R.id.container),
                            "Couldn't decode retrieve data from server",
                            Snackbar.LENGTH_INDEFINITE
                    ).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Snackbar.make(findViewById(R.id.container), "Couldn't retrieve data from server",
                        Snackbar.LENGTH_INDEFINITE).show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                findViewById(R.id.progressbar).setVisibility(View.GONE);
            }
        });
    }


}
