package com.telequto.mutai.veribiz;

import android.content.Context;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by mutai on 5/9/17.
 */

public class Servlet extends HttpServlet {
    private GPSTracker gps;

    Servlet(Context context) {
        gps = new GPSTracker(context);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        resp.setContentType("application/json");
        /*
        Allow from all clients. Very secure if USB tethering is used because only the one connected
        device can make requests to the servlet. Also secure if device is hosting a wifi hotspot as
        long as it is secured with a wifi. Leaves us only with the problem of determining the
        address of the network providing device.
         */
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = resp.getWriter();
        if(req.getRequestURI().equals("/get-coordinates"))
        {
            // Check if GPS enabled
            if(gps.canGetLocation()) {
                gps.getLocation();
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                String coordinates = "{\"success\": true, \"latitude\": " + latitude + ", \"longitude\": " + longitude + "}";
                out.println(coordinates);
            } else {
                out.println("{\"success\": false, \"msg\": \"GPS is disabled on device!\"}");
            }
        }
    }
}
